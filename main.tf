resource "null_resource" "null" {
}

variable "mycount" {
  default =2
}

resource "random_pet" "pet1" {
  prefix = timestamp()
  length = 1
}

output "pet1" {
  value = random_pet.pet1.*.id
}


output "myoutput" {
  value = "MyFirst Repo with the Null Provider"
}